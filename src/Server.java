import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {

    public static final int PORT = 8080;

    public static void main(String[] args) {
        ServerSocket server = null;
        try {
            server = new ServerSocket(PORT);

            while (true) {
                Socket connection = null;
                try {
                    connection = server.accept();
                    BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                    PrintWriter out = new PrintWriter(connection.getOutputStream());

                    out.println("Server start");
                    out.flush();

                    String line = in.readLine();
                    while (line != null && line.length() > 0) {
                        out.println("Request: " + line);
                        out.flush();
                        line = in.readLine();
                    }

                    in.close();
                    out.close();
                    connection.close();
                } catch (IOException exception) {
                    System.out.println("Something wrong!");
                } finally {
                    try {
                        if (connection != null)
                            connection.close();
                    } catch (IOException exception) {
                        System.out.println("Can not close connection");
                    }

                }
            }
        } catch (IOException exception) {
            exception.printStackTrace();
        } finally {
            try {
                if (server != null)
                    server.close();
            } catch (IOException exception) {
                System.out.println("Can not close server");
            }
        }
    }
}