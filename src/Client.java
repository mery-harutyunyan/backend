import java.io.*;
import java.net.Socket;

public class Client {
    public static final String SERVER = "localhost";
    public static final int PORT = 8080;
    public static final int TIMEOUT = 15000;

    public static void main(String[] args) {
        System.out.println("Client start");
        Socket socket = null;
        try {
            socket = new Socket(SERVER, PORT);
            socket.setSoTimeout(TIMEOUT);

            PrintStream out = new PrintStream(socket.getOutputStream());
            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));

            out.println("GET / HTTP/1.0");
            out.println();

            String line = in.readLine();
            while (line != null) {
                System.out.println(line);
                line = in.readLine();
            }

            in.close();
            out.close();
            socket.close();
        } catch (IOException exception) {
            System.err.println(exception);
        } finally {
            if (socket != null) {
                try {
                    socket.close();
                } catch (IOException exception) {
                    System.out.println("Can not close socket");
                }
            }
        }
    }


}